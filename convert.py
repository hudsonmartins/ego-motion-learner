import math
import numpy as np
from scipy.spatial.transform import Rotation

def matrix_to_euler(R):
    r = Rotation.from_matrix(R)
    return r.as_euler('zyx', degrees=False)

def euler_to_matrix(R):
    r = Rotation.from_euler('zyx', R, degrees=False)
    return r.as_matrix()

def kitti_to_6dof(pose):
    pos = np.array([pose[3], pose[7], pose[11]])
    R = np.array([[pose[0], pose[1], pose[2]],
                  [pose[4], pose[5], pose[6]],
                  [pose[8], pose[9], pose[10]]])
    angles = matrix_to_euler(R)
    return np.concatenate((pos, angles))

def prediction_to_kitti(pose):
    pos = np.reshape(pose[:3], (3,1))
    R = pose[3:]
    angles = euler_to_matrix(R)
    return np.concatenate((angles, pos), axis=1)

def translation_to_skew_symmetric(t):
    return np.array([[0, -t[2], t[1]],
                    [t[2], 0, -t[0]],
                    [-t[1], t[0], 0]])


def isRotationMatrix(R) :
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype = R.dtype)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-6
 
def rotationMatrixToEulerAngles(pose) :
    
    pos = np.array([pose[3], pose[7], pose[11]])
    R = np.array([[pose[0], pose[1], pose[2]],
                  [pose[4], pose[5], pose[6]],
                  [pose[8], pose[9], pose[10]]])
    
    assert(isRotationMatrix(R))

    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
    singular = sy < 1e-6
    if(not singular):
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else:
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0
 
    angles = np.array([x, y, z])
    return np.concatenate((pos, angles))