import os
import sys
import torch
import numpy as np
from utils import *
import matplotlib.cm as cm
from convert import rotationMatrixToEulerAngles
from models.matching import Matching
from models.utils import read_image

def arange_like(x, dim: int):
    return x.new_ones(x.shape[dim]).cumsum(0) - 1  # traceable in 1.1

def pad_matrix(arr, max_kpts, both_axis=False):
    #fill with zeros
    if(both_axis):
        return np.pad(arr, [(0, max_kpts-arr.shape[0]), (0, max_kpts-arr.shape[1])])
    else:
        return np.pad(arr, [(0, max_kpts-arr.shape[0]), (0, 0)])

def get_means_and_stds(ys):
    deltas = []
    for seq in ys:
        for i in range(len(seq)-1):
            delta = rotationMatrixToEulerAngles(seq[i+1])-rotationMatrixToEulerAngles(seq[i])
            deltas.append(delta)
    deltas = np.array(deltas)
    means = np.mean(deltas, axis=0)
    stds = np.std(deltas, axis=0)
    return means, stds


def get_iterator(data_path, cycle_every, batch_size, max_kpts, sequences_names, 
    max_skip, random_seed, randomize=True, means=None, stds=None, get_names=False):
    random_seed = random_seed
    rand = np.random.RandomState(random_seed)
    return Dataset(data_path, batch_size=batch_size,  max_kpts=max_kpts, image_size=(640, 480),
        max_skip=max_skip, rand=rand, randomize=randomize, cycle_every=cycle_every, 
        sequences_names=sequences_names, means=means, stds=stds, get_names=get_names)


class Dataset():

    def __init__(self, data_path, batch_size, max_kpts, sequences_names, image_size, max_skip,
        rand, randomize=True, cycle_every=None, means=None, stds=None, get_names=False):

        self._ys = []

        for sequence in sequences_names:
            with open(os.path.join(data_path+'/poses/',  sequence+'.txt')) as file:
                poses = np.array([[float(x) for x in line.split()] for line in file],
                    dtype=np.float32)
            self._ys.append(poses)

        if(means is not None and stds is not None):
            self.means = means
            self.stds = stds
        else:
            self.means, self.stds = get_means_and_stds(self._ys)

        print('means: ', self.means, ' \nstds: ', self.stds)
        self._batch_size = batch_size
        self._rand = rand
        self._rand_state = self._rand.get_state()
        self._data_path = data_path
        self.sequences_names = sequences_names
        self._cycle_every = cycle_every
        self._n_iterations = 0
        self._sequences = list(range(len(self._ys)))
        self._curr_sequence = 0
        self._last_frame = 0
        self._img_size = image_size
        self._max_skip = max_skip+1
        self._max_kpts = max_kpts
        self._randomize = randomize
        self._device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.iter = 0
        self.get_names = get_names
        
    def reset(self):
        self._rand.set_state(self._rand_state)
        self._n_iterations = 0

    def get_batch(self):
        if self._cycle_every is not None and self._n_iterations > 0 \
                and self._n_iterations % self._cycle_every == 0:
            self.reset()
        names = []
        imgs0, imgs1, ys = [], [], []
        for _ in range(self._batch_size):
            #First image
            if(self._randomize):
                self._curr_sequence = self._rand.choice(list(range(len(self._ys))))
            else:
                if(self._last_frame >= len(self._ys[self._curr_sequence])-self._max_skip):
                    self._curr_sequence += 1
                    self._last_frame = 0
                    if(self._curr_sequence == len(self._sequences)):
                        self._curr_sequence = 0

            sequence = self._curr_sequence

            #-------LOADING IMAGES-------
            #get first image
            if(self._randomize):
                index = self._rand.choice(list(range(0, len(self._ys[sequence]))))
            else:
                index = self._last_frame

            image_path = os.path.join(self._data_path+'/sequences/', self.sequences_names[sequence],
                'image_2', '%06d'%index+'.png')
                        
            img0, _, _ = read_image(image_path, self._device, self._img_size, 0, True)

            #Second image
            #Choose a frame between t+1 and t+max_skip
            if(self._randomize):
                pair_index = index + self._rand.choice(list(range(1, self._max_skip)))
            else:
                pair_index = index + 1
            if(pair_index >= len(self._ys[sequence])):
                pair_index = index
            
            #get pair image
            image_path = os.path.join(self._data_path+'/sequences/', self.sequences_names[sequence],
                'image_2', '%06d'%pair_index+'.png')
            if(not os.path.exists(image_path)):
                pair_index = index
                image_path = os.path.join(self._data_path+'/sequences/', self.sequences_names[sequence],
                'image_2', '%06d'%index+'.png')

            img1, _, _ = read_image(image_path, self._device, self._img_size, 0, True)
            self._last_frame = pair_index

            #-------LOADING POSES-------
            #print('images ', index, ' and ', pair_index)
            pose1 = rotationMatrixToEulerAngles(self._ys[sequence][index])
            pose2 = rotationMatrixToEulerAngles(self._ys[sequence][pair_index])
            #Ego-motion
            y = ((pose2-pose1)-self.means)/self.stds
            
            #Appending to batch
            imgs0.append(img0)
            imgs1.append(img1)
            ys.append(y)
            if(self.get_names):
                names.append([index, pair_index])
            self.iter+=1

        imgs0, imgs1, ys = np.stack(imgs0, axis=0), np.stack(imgs1, axis=0), np.stack(ys, axis=0)
        self._n_iterations += 1
        
        if(self.get_names):
            return ((imgs0, imgs1), ys, names)
        return ((imgs0, imgs1), ys)
       
    def iterate(self):
        while True:
            yield self.get_batch()


if __name__ == '__main__':
    rand = np.random.RandomState(42)

    it = get_iterator('/home/hudson/Desktop/Unicamp/Doutorado/Projeto/datasets/kitti/', cycle_every=None, batch_size=1,
        max_kpts=500, sequences_names=['03'], max_skip=1, random_seed=42)
    for x, y in it.iterate():
        print(x[0].shape, x[1].shape)
        print("delta ", y)
        break
        
