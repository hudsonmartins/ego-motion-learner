import os
import glob
import torch
from torch import optim, nn
import numpy as np
from utils import *
from image_loader import get_iterator
from skimage import io
from simple_model import SimpleModel
from tensorboardX import SummaryWriter
import matplotlib.pyplot as plt
from torch import autograd

def weighted_mse(output, target):
    K = 1#100.0
    return torch.mean((output[:, :3] - target[:, :3])**2 + K*(output[:, 3:] - target[:, 3:])**2)

def train_net(model, load_model, data_path, output_path, training_sequences, validation_sequences,
    logs_dir, checkpoints_dir, batch_size, lr, epochs, steps_per_epoch, random_seed,
    early_stopping=True, save_best_only=True, model_name="best_model"):

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    writer = SummaryWriter(logs_dir,
        comment= "_LR_"+ str(lr) + "_Batch_size_" + str(batch_size)) #tensorboard writer will output to logs directory
    print(f'Device found: {device}')
    print(f'Batch size: {batch_size}\nLearning rate: {lr}')
    optimizer = optim.Adam(model.parameters(), lr=lr)
    criterion = nn.MSELoss()        
    train_iterator = get_iterator(data_path, max_skip=1, cycle_every=None, max_kpts=500,
        batch_size=batch_size, sequences_names=training_sequences, random_seed=random_seed)
    
    epoch = 0
    best_loss = None
    best_epoch = -1
    epoch_loss = 0
    i = 0
    model.to(device)

    if(load_model):
        checkpoint = torch.load(load_model)
        model.superpoint.load_state_dict(checkpoint['superpoint'])
        model.superglue.load_state_dict(checkpoint['superglue'])
        model.output.load_state_dict(checkpoint['output'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        epoch = checkpoint['epoch'] + 1
        loss = checkpoint['loss']
    
    print('Starting epoch ', epoch)
    with autograd.detect_anomaly():
        for x, y in train_iterator.iterate():
            model.train()
            
            img0 = torch.from_numpy(x[0]/255).float().unsqueeze(1).to(device)
            img1 = torch.from_numpy(x[1]/255).float().unsqueeze(1).to(device)
            optimizer.zero_grad()
            pred = model(img0, img1)

            y = torch.Tensor(y).to(device)
            loss = criterion(pred, y)
            #print("loss ", loss)
            epoch_loss += loss.item()
            loss.backward()
            """
            for name, param in model.named_parameters():
                print(name)
                if(param.grad != None):
                    print(param.grad.abs().sum())
                else:
                    print("None")
            """
            optimizer.step()
            i += 1
            print(f'{i}/{steps_per_epoch}')
            
            if(i%steps_per_epoch == 0):
                print(f'Epoch {epoch} finished. Loss: {epoch_loss/i}')
                title = 'Epoch_loss/train Parameters: '+' LR = ' + str(lr) + ' Batch_size = ' + str(batch_size)
                writer.add_scalar(title, (epoch_loss/i), epoch)
                
                """
                print("-"*20)
                print('STARTING VALIDATION')
                val_loss = 0
                val_mse = 0
                validation_size = steps_per_epoch
                means = train_iterator.means
                stds = train_iterator.stds
                valid_iterator = get_iterator(data_path, max_skip=1, cycle_every=validation_size, max_kpts=500,
                    batch_size=batch_size, sequences_names=validation_sequences, randomize=False, means=means, 
                    stds=stds, random_seed=random_seed)
                #---------------------Validation--------------------------
                with torch.no_grad():
                    i = j = 0
                    for x, y in valid_iterator.iterate():
                        descs = torch.from_numpy(x[0]).to(device)
                        kpts = torch.from_numpy(x[1]).to(device)
                        model.eval()
                        pred = model(descs.float(), kpts.float())
                        y = torch.Tensor(y).to(device)
                        mse = criterion(pred, y)
                        wmse = weighted_mse(pred, y)
                        val_loss += wmse.item()
                        val_mse += mse.item()
                        j += 1
                        print(f'{j}/{validation_size}')

                        if(j == validation_size):
                            break
                    print(f'validation loss: {val_loss/j}')
                    print(f'validation mse: {val_mse/j}')
                title = 'Loss/validation Parameters: '+' LR = '+str(lr)+' Batch_size = '+str(batch_size)
                writer.add_scalar(title, (val_loss/j), epoch)
                writer.flush()
                
                print("="*20)
                if(best_loss is not None):
                    if((val_loss/j) < best_loss):
                        best_loss = (val_loss/j)
                        best_epoch = epoch

                        if(save_best_only):
                            torch.save(model.state_dict(), os.path.join(checkpoints_dir, model_name))
                            print(f'Best model {epoch} saved!')

                    if(not save_best_only):
                        torch.save(model.state_dict(), os.path.join(checkpoints_dir,  model_name+"_"+str(epoch)+".pth"))
                        print(f'Model {epoch} saved!')
                else:
                    best_loss = (val_loss/j)
                    best_epoch = epoch
                    torch.save(model.state_dict(), os.path.join(checkpoints_dir, model_name+"_"+str(epoch)+".pth"))
                    print(f'Model 0 saved!')
                """
                

                output_name = model_name+"_"+str(epoch)
                
                if(best_loss is not None):
                    if((epoch_loss/i) < best_loss):
                        best_loss = epoch_loss/i
                        best_epoch = epoch

                        if(save_best_only):
                            torch.save({
                                'epoch': epoch,
                                'superpoint': model.superpoint.state_dict(),
                                'superglue': model.superglue.state_dict(),
                                'output': model.output.state_dict(),
                                'optimizer': optimizer.state_dict(),
                                'loss': loss,
                                }, os.path.join(checkpoints_dir, output_name+".pth"))
                            #model.save(os.path.join(checkpoints_dir,  output_name))
                            print(f'Model {output_name} saved!')

                    if(not save_best_only):
                        #model.save(os.path.join(checkpoints_dir,  output_name))
                        torch.save({
                                'epoch': epoch,
                                'superpoint': model.superpoint.state_dict(),
                                'superglue': model.superglue.state_dict(),
                                'output': model.output.state_dict(),
                                'optimizer': optimizer.state_dict(),
                                'loss': loss,
                                }, os.path.join(checkpoints_dir, output_name+".pth"))
                        print(f'Model {output_name} saved!')
                else:
                    best_loss = epoch_loss/i
                    best_epoch = epoch
                    #model.save(os.path.join(checkpoints_dir,  output_name))
                    torch.save({
                                'epoch': epoch,
                                'superpoint': model.superpoint.state_dict(),
                                'superglue': model.superglue.state_dict(),
                                'output': model.output.state_dict(),
                                'optimizer': optimizer.state_dict(),
                                'loss': loss,
                                }, os.path.join(checkpoints_dir, output_name+".pth"))
                    print(f'Model {output_name} saved!')
                
                #print(f'Best validation loss {best_loss} in epoch {best_epoch}')
                print(f'Best training loss {best_loss} in epoch {best_epoch}')
                epoch += 1
                epoch_loss = 0
                i = 0

                print(f'Starting epoch {epoch}')

            


def main(data_path, output_path, batch_size, lr, epochs, steps_per_epoch, load_model, random_seed, model_name):
    
    logs_dir = os.path.join(output_path, 'logs')
    checkpoints_dir = os.path.join(output_path, 'models')
    os.makedirs(logs_dir, exist_ok=True)
    os.makedirs(checkpoints_dir, exist_ok=True)
    training_sequences = ['00', '02', '04', '05', '06', '08', '09']
    validation_sequences = ['10']

    model = SimpleModel()
    #if(load_model):
    #    model.load(str(load_model).replace(".pth",""), train=True)

    train_net(model, load_model, data_path, output_path, training_sequences, validation_sequences,
        logs_dir, checkpoints_dir, batch_size, lr, epochs, steps_per_epoch, random_seed,
        early_stopping=True, save_best_only=False, model_name=model_name)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("inputs", default=None, help="Path to images and labels")
    parser.add_argument("outputs", default=None, help="Path to output data")
    parser.add_argument("--steps_per_epoch", default=500, type=int)
    parser.add_argument("--epochs", default=100, type=int)
    parser.add_argument("--batch_size", default=32, type=int)
    parser.add_argument("--lr", default=0.001, type=float)
    parser.add_argument("--random_seed", default=42, type=int)
    parser.add_argument("--load_model", default=None)
    parser.add_argument("--save_model_name", default="best_model")
    args = parser.parse_args()

    main(args.inputs, args.outputs, args.batch_size, args.lr, args.epochs,
        args.steps_per_epoch, args.load_model, args.random_seed, args.save_model_name)
