import torch
from torch import nn
import torch.nn.functional as F
from models.utils import frame2tensor
from models.superpoint import SuperPoint
from models.superglue import SuperGlue, normalize_keypoints
from models.matching import Matching


class Output(nn.Module):
    def __init__(self):
        super(Output, self).__init__()
        self.fc1 = nn.Linear(3000, 2048)
        self.bn1 = nn.BatchNorm1d(2048) 
        
        self.fc2 = nn.Linear(2048, 1024)
        self.bn2 = nn.BatchNorm1d(1024) 
        
        self.fc3 = nn.Linear(1024, 512)
        self.bn3 = nn.BatchNorm1d(512) 

        self.fc4 = nn.Linear(512, 256)
        self.bn4 = nn.BatchNorm1d(256) 

        self.fc5 = nn.Linear(256, 128)
        self.bn5 = nn.BatchNorm1d(128) 

        self.fc6 = nn.Linear(128, 6)
        self.relu = nn.ReLU()

    def forward(self, kpts0, kpts1, matches0, matches1, scores0, scores1):
        matches0 = torch.unsqueeze(matches0, -1)
        #matches1 = torch.unsqueeze(matches1, -1)
        scores0 = torch.unsqueeze(scores0, -1)
        #scores1 = torch.unsqueeze(scores1, -1)
        #x = torch.cat((kpts0, kpts1, matches0, matches1, scores0, scores1), 2)
        x = torch.cat((kpts0, kpts1, matches0, scores0), 2)

        x = x.view(-1, 3000)
        x = self.fc1(x)
        x = self.bn1(x)
        x = self.relu(x)

        x = self.fc2(x)
        x = self.bn2(x)
        x = self.relu(x)

        x = self.fc3(x)
        x = self.bn3(x)
        x = self.relu(x)

        x = self.fc4(x)
        x = self.bn4(x)
        x = self.relu(x)

        x = self.fc5(x)
        x = self.bn5(x)
        x = self.relu(x)

        x = self.fc6(x)
        return x

class SimpleModel(nn.Module):
    def __init__(self):
        super(SimpleModel, self).__init__()
        self.config = {
                        'superpoint': {
                            'nms_radius': 4,
                            'keypoint_threshold': 0.005,
                            'max_keypoints': 500
                        },
                        'superglue': {
                            'weights': 'outdoor',
                            'sinkhorn_iterations': 20,
                            'match_threshold': 0.2,
                        }
                    }

        self.superpoint = SuperPoint(self.config.get('superpoint', {})).train()
        self.superglue = SuperGlue(self.config.get('superglue', {})).train()
        self.output = Output().train()
    
    def forward(self, inp0, inp1):
        pred = {}
        x0 = self.superpoint({'image': inp0})
        pred = {**pred, **{k+'0': v for k, v in x0.items()}}
        x1 = self.superpoint({'image': inp1})
        pred = {**pred, **{k+'1': v for k, v in x1.items()}}
        data = {**{'image0': inp0, 'image1': inp1}, **pred}

        for k in data:
            if isinstance(data[k], (list, tuple)):
                new_data = []
                if(k.startswith('keypoints')):
                    #padding keypoints
                    for kpt in data[k]:
                        new_data += [F.pad(kpt, 
                                    (0, 0, 0, self.config['superpoint']['max_keypoints'] - kpt.shape[0]))]

                if(k.startswith('descriptor')):
                    #padding descriptors
                    for desc in data[k]:
                        new_data += [F.pad(desc, 
                                    (0, self.config['superpoint']['max_keypoints'] - desc.shape[1]))]

                if(k.startswith('score')):
                    #padding scores
                    for score in data[k]:
                        new_data += [F.pad(score, 
                                    (0, self.config['superpoint']['max_keypoints'] - score.shape[0]))]
                data[k] = torch.stack(new_data)

        matches = self.superglue(data)

        kpts0 = normalize_keypoints(data['keypoints0'], 
                                    [0, 0, inp0.shape[-2], 
                                    inp0.shape[-1]])

        matches0 = matches['matches0']/inp0.shape[-1]

        scores0 = matches['matching_scores0']

        kpts1 = normalize_keypoints(data['keypoints1'], 
                                    [0, 0, inp1.shape[-2], 
                                    inp1.shape[-1]])

        matches1 = matches['matches1']/inp1.shape[-1]                       
        scores1 = matches['matching_scores1']
        x = self.output(kpts0, kpts1, matches0, matches1, scores0, scores1)
        return x


if __name__ == '__main__':
    import time
    import numpy as np
    from image_loader import get_iterator
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = SimpleModel()
    model.to(device)
    it = get_iterator('/home/hudson/Desktop/Unicamp/Doutorado/Projeto/datasets/kitti/', 
                    cycle_every=None, batch_size=2, max_kpts=500, sequences_names=['03'], max_skip=1, random_seed=42)
    
    for x, y in it.iterate():
        # (N, C, H, W)
        inp0 = torch.from_numpy(x[0]/255).float().unsqueeze(1).to(device)
        inp1 = torch.from_numpy(x[1]/255).float().unsqueeze(1).to(device)
        
        init = time.time()
        # A full forward pass
        x = model(inp0, inp1)
        #print("out ", x.shape)
        print(time.time() - init)
        #break