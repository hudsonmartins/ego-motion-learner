import os
import sys
import torch
import numpy as np
from utils import *
import matplotlib.cm as cm
from convert import rotationMatrixToEulerAngles
from models.matching import Matching
from models.superglue import normalize_keypoints
from models.utils import make_matching_plot_fast, read_image


def log_sinkhorn_iterations(Z, log_mu, log_nu, iters: int):
    """ Perform Sinkhorn Normalization in Log-space for stability"""
    u, v = torch.zeros_like(log_mu), torch.zeros_like(log_nu)
    for _ in range(iters):
        u = log_mu - torch.logsumexp(Z + v.unsqueeze(1), dim=2)
        v = log_nu - torch.logsumexp(Z + u.unsqueeze(2), dim=1)
    return Z + u.unsqueeze(2) + v.unsqueeze(1)


def log_optimal_transport(scores, alpha, iters: int):
    """ Perform Differentiable Optimal Transport in Log-space for stability"""
    b, m, n = scores.shape
    one = scores.new_tensor(1)
    ms, ns = (m*one).to(scores), (n*one).to(scores)

    bins0 = alpha.expand(b, m, 1)
    bins1 = alpha.expand(b, 1, n)
    alpha = alpha.expand(b, 1, 1)

    couplings = torch.cat([torch.cat([scores, bins0], -1),
                           torch.cat([bins1, alpha], -1)], 1)

    norm = - (ms + ns).log()
    log_mu = torch.cat([norm.expand(m), ns.log()[None] + norm])
    log_nu = torch.cat([norm.expand(n), ms.log()[None] + norm])
    log_mu, log_nu = log_mu[None].expand(b, -1), log_nu[None].expand(b, -1)

    Z = log_sinkhorn_iterations(couplings, log_mu, log_nu, iters)
    Z = Z - norm  # multiply probabilities by M+N
    return Z


def arange_like(x, dim: int):
    return x.new_ones(x.shape[dim]).cumsum(0) - 1  # traceable in 1.1

def pad_matrix(arr, max_kpts, both_axis=False):
    #fill with zeros
    if(both_axis):
        return np.pad(arr, [(0, max_kpts-arr.shape[0]), (0, max_kpts-arr.shape[1])])
    else:
        return np.pad(arr, [(0, max_kpts-arr.shape[0]), (0, 0)])

def get_means_and_stds(ys):
    deltas = []
    for seq in ys:
        for i in range(len(seq)-1):
            delta = rotationMatrixToEulerAngles(seq[i+1])-rotationMatrixToEulerAngles(seq[i])
            deltas.append(delta)
    deltas = np.array(deltas)
    means = np.mean(deltas, axis=0)
    stds = np.std(deltas, axis=0)
    """dist_from_mean = abs(deltas - means)
    mask = dist_from_mean < 2 * stds
    mask = np.all(mask, axis=1)
    deltas = deltas[mask]
    means = np.mean(deltas, axis=0)
    stds = np.std(deltas, axis=0)"""
    return means, stds


def get_iterator(data_path, cycle_every, batch_size, max_kpts, sequences_names, 
    max_skip, random_seed, randomize=True, means=None, stds=None, get_names=False):
    random_seed = random_seed
    rand = np.random.RandomState(random_seed)
    return Dataset(data_path, batch_size=batch_size,  max_kpts=max_kpts, image_size=(640, 480),
        max_skip=max_skip, rand=rand, randomize=randomize, cycle_every=cycle_every, 
        sequences_names=sequences_names, means=means, stds=stds, get_names=get_names)


class Dataset():

    def __init__(self, data_path, batch_size, max_kpts, sequences_names, image_size, max_skip,
        rand, randomize=True, cycle_every=None, means=None, stds=None, get_names=False):

        self._ys = []
        self.config = {
                        'superpoint': {
                            'nms_radius': 4,
                            'keypoint_threshold': 0.005,
                            'max_keypoints': max_kpts
                        },
                        'superglue': {
                            'weights': 'outdoor',
                            'sinkhorn_iterations': 20,
                            'match_threshold': 0.2,
                        }
                    }

        for sequence in sequences_names:
            with open(os.path.join(data_path+'/poses/',  sequence+'.txt')) as file:
                poses = np.array([[float(x) for x in line.split()] for line in file],
                    dtype=np.float32)
            self._ys.append(poses)

        if(means is not None and stds is not None):
            self.means = means
            self.stds = stds
        else:
            self.means, self.stds = get_means_and_stds(self._ys)

        print('means: ', self.means, ' \nstds: ', self.stds)
        self._batch_size = batch_size
        self._rand = rand
        self._rand_state = self._rand.get_state()
        self._data_path = data_path
        self.sequences_names = sequences_names
        self._cycle_every = cycle_every
        self._n_iterations = 0
        self._sequences = list(range(len(self._ys)))
        self._curr_sequence = 0
        self._last_frame = 0
        self._img_size = image_size
        self._max_skip = max_skip+1
        self._max_kpts = max_kpts
        self._randomize = randomize
        self._device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self._matching = Matching(self.config).eval().to(self._device )
        self.show_matches = False
        self.iter = 0
        self.get_names = get_names
        
    def reset(self):
        self._rand.set_state(self._rand_state)
        self._n_iterations = 0

    def get_batch(self):
        if self._cycle_every is not None and self._n_iterations > 0 \
                and self._n_iterations % self._cycle_every == 0:
            self.reset()
        names = []
        kpts, scores, ys = [], [], []
        for _ in range(self._batch_size):
            #First image
            if(self._randomize):
                self._curr_sequence = self._rand.choice(list(range(len(self._ys))))
            else:
                if(self._last_frame >= len(self._ys[self._curr_sequence])-self._max_skip):
                    self._curr_sequence += 1
                    self._last_frame = 0
                    if(self._curr_sequence == len(self._sequences)):
                        self._curr_sequence = 0

            sequence = self._curr_sequence

            #-------LOADING IMAGES-------
            #get first image
            if(self._randomize):
                index = self._rand.choice(list(range(0, len(self._ys[sequence]))))
            else:
                index = self._last_frame

            image_path = os.path.join(self._data_path+'/sequences/', self.sequences_names[sequence],
                'image_2', '%06d'%index+'.png')
                        
            img0, inp0, scales0 = read_image(image_path, self._device, self._img_size, 0, True)

            #Second image
            #Choose a frame between t+1 and t+max_skip
            if(self._randomize):
                pair_index = index + self._rand.choice(list(range(1, self._max_skip)))
            else:
                pair_index = index + 1
            if(pair_index >= len(self._ys[sequence])):
                pair_index = index
            
            #get pair image
            image_path = os.path.join(self._data_path+'/sequences/', self.sequences_names[sequence],
                'image_2', '%06d'%pair_index+'.png')
            if(not os.path.exists(image_path)):
                pair_index = index
                image_path = os.path.join(self._data_path+'/sequences/', self.sequences_names[sequence],
                'image_2', '%06d'%index+'.png')

            img1, inp1, scales1 = read_image(image_path, self._device, self._img_size, 0, True)
            self._last_frame = pair_index

            pred = self._matching({'image0': inp0, 'image1': inp1})

            kpts0 = normalize_keypoints(pred['keypoints0'][0], [0, 0, img0.shape[0], img0.shape[1]])
            kpts1 = normalize_keypoints(pred['keypoints1'][0], [0, 0, img1.shape[0], img1.shape[1]])

            #Convert to input
            kpts0 = kpts0[0].detach().cpu().numpy()
            if(len(kpts0) < self._max_kpts):
                kpts0 = pad_matrix(kpts0, self._max_kpts)
            
            kpts1 = kpts1[0].detach().cpu().numpy()
            if(len(kpts1) < self._max_kpts):
                kpts1 = pad_matrix(kpts1, self._max_kpts)
            
            matching_scores = pred['matching_scores'][0].detach().cpu().numpy()
            if(matching_scores.shape[0] < self._max_kpts or matching_scores.shape[1] < self._max_kpts):
                matching_scores = pad_matrix(matching_scores, self._max_kpts, both_axis=True)
            
            if(self.show_matches):
                bin_score = torch.nn.Parameter(torch.tensor(1.)).to(self._device)
                scores_tensor = np.array([matching_scores])
                scores_tensor = torch.from_numpy(scores_tensor).to(self._device)
                scores_tensor = log_optimal_transport(scores_tensor, bin_score,
                                               iters=20)
                max0, max1 = scores_tensor[:, :-1, :-1].max(2), scores_tensor[:, :-1, :-1].max(1)
                indices0, indices1 = max0.indices, max1.indices
                mutual0 = arange_like(indices0, 1)[None] == indices1.gather(1, indices0)
                mutual1 = arange_like(indices1, 1)[None] == indices0.gather(1, indices1)
                zero = scores_tensor.new_tensor(0)
                mscores0 = torch.where(mutual0, max0.values.exp(), zero)
                mscores1 = torch.where(mutual1, mscores0.gather(1, indices1), zero)

                valid0 = mutual0 & (mscores0 > 0.2)
                valid1 = mutual1 & valid0.gather(1, indices1)
                indices0 = torch.where(valid0, indices0, indices0.new_tensor(-1))
                indices1 = torch.where(valid1, indices1, indices1.new_tensor(-1))
                confidence = mscores0[0].cpu().detach().numpy()
                matches = indices0[0].cpu().detach().numpy()
                valid = matches > -1

                mkpts0 = pred['keypoints0'][0].detach().cpu().numpy()[valid]
                mkpts1 = pred['keypoints1'][0].detach().cpu().numpy()[matches[valid]]
                color = cm.jet(confidence[valid])

                text = ['SuperGlue',
                        'Keypoints: {}:{}'.format(len(kpts0), len(kpts1)),
                        'Matches: {}'.format(len(mkpts0)),]
                k_thresh = 0.005
                m_thresh = 0.2
                small_text = ['Keypoint Threshold: {:.4f}'.format(k_thresh),
                            'Match Threshold: {:.2f}'.format(m_thresh),]
        
                make_matching_plot_fast(img0, img1, pred['keypoints0'][0].detach().cpu().numpy(), pred['keypoints1'][0].detach().cpu().numpy(), mkpts0, mkpts1, color, text=text, small_text=small_text,
                path='/home/hudson/Desktop/Unicamp/Doutorado/Projeto/Codigos/ego-motion-learner/matches/matches-'+str(self.iter)+'.png', 
                show_keypoints=True)

            #-------LOADING POSES-------
            #print('images ', index, ' and ', pair_index)
            pose1 = rotationMatrixToEulerAngles(self._ys[sequence][index])
            pose2 = rotationMatrixToEulerAngles(self._ys[sequence][pair_index])
            #Ego-motion
            y = ((pose2-pose1)-self.means)/self.stds
            
            #Appending to batch
            kpts.append([kpts0, kpts1])
            scores.append(matching_scores)
            ys.append(y)
            if(self.get_names):
                names.append([index, pair_index])
            self.iter+=1

        kpts, scores, ys = np.stack(kpts, axis=0), np.stack(scores, axis=0), np.stack(ys, axis=0)
        self._n_iterations += 1
        
        if(self.get_names):
            return ((kpts, scores), ys, names)
        return ((kpts, scores), ys)
       
    def iterate(self):
        while True:
            yield self.get_batch()


if __name__ == '__main__':
    rand = np.random.RandomState(42)

    it = get_iterator('/home/hudson/Desktop/Unicamp/Doutorado/Projeto/datasets/kitti/', cycle_every=None, batch_size=1,
        max_kpts=500, sequences_names=['03'], max_skip=1, random_seed=42)
    for x, y in it.iterate():
        print(x[0].shape, x[1].shape)
        print("delta ", y)

        break
        
